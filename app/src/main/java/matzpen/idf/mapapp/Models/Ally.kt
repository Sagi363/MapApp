package matzpen.idf.mapapp

import com.google.gson.annotations.Expose
import retrofit2.http.Field

/**
 * Created by sagihatzabi on 11/26/17.
 */
data class Ally(@Expose @Field("mi") val persNumber: String, val id: String, @Expose @Field("location") var location: Location, val timestamp: Long, val url: String, val name: String?)