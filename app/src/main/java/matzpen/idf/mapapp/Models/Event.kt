package matzpen.idf.mapapp

import java.util.*

/**
 * Created by sagihatzabi on 11/26/17.
 */
data class Event(var listID: Long, val id: String, val location: Location, val timestamp: Long, val severity: Int, val name: String, val description: String)