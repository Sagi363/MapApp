package matzpen.idf.mapapp

import android.location.Location
import retrofit2.http.Field
import java.util.*

/**
 * Created by sagihatzabi on 11/26/17.
 */
data class Location(@Field("lat") val lat: Float, @Field("long") val long: Float)