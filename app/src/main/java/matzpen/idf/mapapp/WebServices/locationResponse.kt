package matzpen.idf.mapapp.WebServices

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import matzpen.idf.mapapp.Location
import java.util.*


/**
 * Created by sagihatzabi on 11/10/17.
 */
class locationResponse {

    @SerializedName("lat")
    @Expose
    var lat: Long? = null

    @SerializedName("long")
    @Expose
    var long: Long? = null
}