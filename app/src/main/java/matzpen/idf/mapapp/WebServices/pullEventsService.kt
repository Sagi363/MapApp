package matzpen.idf.mapapp.WebServices

import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import matzpen.idf.mapapp.Ally
import matzpen.idf.mapapp.Event
import matzpen.idf.mapapp.Location
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import okhttp3.logging.HttpLoggingInterceptor


/**
 * Created by sagihatzabi on 11/10/17.
 */
interface pullEventsService {

    @GET("events")
    fun getEvents(): Observable<eventsResponse>

    @GET("friends")
    fun getAllies(): Observable<alliesResponse>

    @POST("friends")
    fun sendLocation(@Body ally: Ally): Observable<allyResponse>

    @POST("events")
    fun sendEvent(@Body event: Event): Observable<eventResponse>

    companion object {

        var BASE_URL: String = "https://mapapp.now.sh/api/"

        fun initServerAddress(url: String) {
            BASE_URL = url
        }

        val eventsService by lazy {
            pullEventsService.create()
        }

        var disposable: Disposable? = null

        fun create(): pullEventsService {

//            var logger = HttpLoggingInterceptor()
//                    .setLevel(HttpLoggingInterceptor.Level.BODY)

//            var client = OkHttpClient
//                    .Builder()
//                    .addInterceptor(logger)
//                    .build()

            val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
//                    .client(client)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            return retrofit.create(pullEventsService::class.java)
        }
    }
}