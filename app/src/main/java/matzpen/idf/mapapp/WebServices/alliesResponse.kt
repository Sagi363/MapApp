package matzpen.idf.mapapp.WebServices

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import matzpen.idf.mapapp.Ally
import matzpen.idf.mapapp.Event
import matzpen.idf.mapapp.Location
import java.util.*


/**
 * Created by sagihatzabi on 11/10/17.
 */
class alliesResponse {

    @SerializedName("data")
    @Expose
    var data: Array<Ally>? = null
}