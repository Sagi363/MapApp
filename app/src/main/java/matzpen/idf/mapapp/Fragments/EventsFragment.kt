package matzpen.idf.mapapp

import android.app.Fragment
import android.location.Location
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.events_fragment.view.*
import java.util.*
import android.support.v7.widget.DividerItemDecoration
import android.widget.Toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import matzpen.idf.mapapp.Events.NewReportEvent
import matzpen.idf.mapapp.WebServices.pullEventsService
import matzpen.idf.mapapp.WebServices.pullEventsService.Companion.disposable
import matzpen.idf.mapapp.WebServices.pullEventsService.Companion.eventsService
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import com.h6ah4i.android.widget.advrecyclerview.swipeable.RecyclerViewSwipeManager
import matzpen.idf.mapapp.Events.RemovedItemsEvent


/**
 * Created by sagihatzabi on 11/7/17.
 */
class EventsFragment : Fragment(), View.OnClickListener {

    lateinit var mNoEventsText: TextView
    lateinit var mRecyclerView: RecyclerView
    private var eventList = mutableListOf<Event>()
    lateinit var mAdapter: EventsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.setRetainInstance(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.events_fragment, container, false)
    }

    override fun onViewCreated(rootView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(rootView, savedInstanceState)

        mRecyclerView = rootView.events_recyclerview
        mNoEventsText = rootView.zero_events_text


        mAdapter = EventsAdapter(eventList, activity.baseContext)
        val mLayoutManager = LinearLayoutManager(activity.applicationContext)
        mLayoutManager.isAutoMeasureEnabled = false
//        mRecyclerView.setLayoutManager(mLayoutManager)
        mRecyclerView.setItemAnimator(DefaultItemAnimator())
        mRecyclerView.addItemDecoration(DividerItemDecoration(activity.baseContext, LinearLayoutManager.VERTICAL))
//        mRecyclerView.setAdapter(mAdapter)

        // Setup swiping feature and RecyclerView
        val swipeMgr = RecyclerViewSwipeManager()

        mRecyclerView.setLayoutManager(mLayoutManager)
        mRecyclerView.setAdapter(swipeMgr.createWrappedAdapter(mAdapter))

        swipeMgr.attachRecyclerView(mRecyclerView)

//        pullLastEventsFromServer()
    }

    private fun pullLastEventsFromServer() {
        disposable =
                eventsService.getEvents()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    result -> updateEvents(result.data)
                                },
                                {
                                    error -> Toast.makeText(activity.baseContext, "error: ${error.message}", Toast.LENGTH_LONG).show()
                                }
                        )

        //showNoEventsText()
    }

    private fun updateRecyleViewLayoutParams() {

        try {
            val scale = activity.baseContext.resources.displayMetrics.density
            var pixels = ((eventList.size * 121) * scale + 0.5f).toInt()

            if (eventList.size > 5) {
                mRecyclerView.layoutParams = mRecyclerView.layoutParams.apply { height = ((5 * 121) * scale + 0.5f).toInt() }
            }
            else {
                mRecyclerView.layoutParams = mRecyclerView.layoutParams.apply { height = pixels }
            }
        }
        catch (e: Exception) {

        }
        finally {
            mAdapter.notifyDataSetChanged()
        }
    }

    private fun updateEvents(eventsFromResult: Array<Event>?) {
        for (event in eventsFromResult!!.toList()) {
            eventList.add(event)
        }

        updateRecyleViewLayoutParams()
    }

    private fun addEvent(event: Event) {
        eventList.add(event)
        updateRecyleViewLayoutParams()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onNewPointEvent(event: Any) {
        when (event) {
            is NewReportEvent -> addEvent(event.report)
            is RemovedItemsEvent -> updateRecyleViewLayoutParams()
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    private fun showNoEventsText() {
        // Show the "No events" text
        if (mNoEventsText != null) {
            mNoEventsText.visibility = View.VISIBLE
        }
        else {
            TODO("not implemented - noEventsText == null")
        }
    }

    override fun onClick(view: View?) {

    }


    companion object {
        fun newInstance(): Fragment {
            val fragment = EventsFragment()
            val args = Bundle()
//            args.putString()
            fragment.arguments = args

            return fragment
        }
    }
}