package matzpen.idf.mapapp.Fragments

import android.app.Fragment
import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import matzpen.idf.mapapp.Event
import matzpen.idf.mapapp.Location
import matzpen.idf.mapapp.MapFragment
import matzpen.idf.mapapp.R
import matzpen.idf.mapapp.WebServices.pullEventsService
import java.util.*


/**
 * Created by sagihatzabi on 12/4/17.
 */
class DialogReportFragment : DialogFragment(), View.OnClickListener {
    override fun onClick(view: View) {
        yellowButton.background = null
        orangeButton.background = null
        redButton.background = null

        when (view.id) {
            R.id.report_event_button_yellow_background -> {
                changeSelectedBackground(severity = 3)
            }
            R.id.report_event_button_orange_background -> {
                changeSelectedBackground(severity = 2)
            }
            R.id.report_event_button_red_background -> {
                changeSelectedBackground(severity = 1)
            }
            R.id.report_event_fragment_button_send -> {
                sendEvent()
            }
        }
    }

    private fun sendEvent() {
        name = nameEditText.text.toString()
        description = descriptionEditText.text.toString()
        var event = Event(0L,"",
                location = Location(lat = lat, long = long),
                severity = severity,
                name = name,
                description = description,
                timestamp = Date().time)

        pullEventsService.disposable =
                pullEventsService.eventsService.sendEvent(event)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    result -> dismiss()
                                },
                                {
                                    error -> Toast.makeText(activity!!.baseContext, "error: ${error.message}", Toast.LENGTH_LONG).show()
                                }
                        )
    }

    private lateinit var yellowButton: LinearLayout
    private lateinit var orangeButton: LinearLayout
    private lateinit var redButton: LinearLayout
    private lateinit var sendButton: Button
    private lateinit var backgroundDrawable: Drawable
    private lateinit var nameEditText: EditText
    private lateinit var descriptionEditText: EditText
    private var lat: Float = 0f
    private var long: Float = 0f
    private var severity: Int = 0
    private var name: String = ""

    private var description: String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.report_dialog_fragment, container, false)

        yellowButton = rootView.findViewById<LinearLayout>(R.id.report_event_button_yellow_background)
        orangeButton = rootView.findViewById<LinearLayout>(R.id.report_event_button_orange_background)
        redButton = rootView.findViewById<LinearLayout>(R.id.report_event_button_red_background)
        sendButton = rootView.findViewById<Button>(R.id.report_event_fragment_button_send)
        nameEditText = rootView.findViewById<EditText>(R.id.report_event_fragment_name)
        descriptionEditText = rootView.findViewById<EditText>(R.id.report_event_fragment_text)
        backgroundDrawable = resources.getDrawable(R.drawable.selected_event_background)

        yellowButton.background = null
        orangeButton.background = null
        redButton.background = null

        yellowButton.setOnClickListener(this)
        orangeButton.setOnClickListener(this)
        redButton.setOnClickListener(this)
        sendButton.setOnClickListener(this)

        if (arguments != null) {
            severity = arguments!!.getInt("severity")
            lat = arguments!!.getFloat("lat")
            long = arguments!!.getFloat("long")
        }

        changeSelectedBackground(severity)

        return rootView
    }

    private fun changeSelectedBackground(severity: Int) {
        var backgroundColor: Int

        when (severity) {
            1 -> {
                redButton.background = backgroundDrawable
                backgroundColor = R.drawable.report_event_button_send_red
            }
            2 -> {
                orangeButton.background = backgroundDrawable
                backgroundColor = R.drawable.report_event_button_send_orange
            }
            3 -> {
                yellowButton.background = backgroundDrawable
                backgroundColor = R.drawable.report_event_button_send_yellow
            }
            else -> backgroundColor = R.color.report_gray
        }

        sendButton.background = resources.getDrawable(backgroundColor)
    }

    companion object {
        fun newInstance(severity: Int, location: Location): DialogFragment {
            val dialogFragment = DialogReportFragment()
            val args = Bundle()
            args.putInt("severity", severity)
            args.putFloat("lat", location.lat)
            args.putFloat("long", location.long)
            dialogFragment.arguments = args

            return dialogFragment
        }
    }
}