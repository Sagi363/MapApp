package matzpen.idf.mapapp

import android.Manifest
import android.app.Fragment
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.widget.TextView
import java.util.*
import android.view.*
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.widget.Toast
import com.esri.arcgisruntime.geometry.GeometryEngine
import com.esri.arcgisruntime.geometry.Point
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import matzpen.idf.mapapp.WebServices.pullEventsService.Companion.disposable
import matzpen.idf.mapapp.WebServices.pullEventsService.Companion.eventsService
import com.esri.arcgisruntime.mapping.Basemap
import com.esri.arcgisruntime.mapping.ArcGISMap
import com.esri.arcgisruntime.geometry.SpatialReferences
import com.esri.arcgisruntime.layers.ArcGISMapImageLayer
import com.esri.arcgisruntime.mapping.view.*
import com.esri.arcgisruntime.symbology.PictureMarkerSymbol
import matzpen.idf.mapapp.Events.NewReportEvent
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat


/**
 * Created by sagihatzabi on 11/7/17.
 */
class MapFragment : Fragment(), View.OnClickListener {

    fun Double.toBigDecimal(): BigDecimal = BigDecimal.valueOf(this)

    lateinit var utils: Utils

    override fun onClick(view: View) {
        val anim = ScaleAnimation(
                0.9f, 1.1f, // Start and end values for the X axis scaling
                0.9f, 1.1f, // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0.5f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 0.7f) // Pivot point of Y scaling
        anim.fillAfter = false // Needed to keep the result of the animation
        anim.duration = 150
//                anim.interpolator =

        anim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}

            override fun onAnimationEnd(animation: Animation) {
                when (view.id) {
                    R.id.report_event_button -> {
                        utils = Utils(activity)
                        utils.readPersNumberFromSharedPrefs()

                        val myPersNumber = utils.persNumber.toString()

                        var ally = Ally(myPersNumber,
                                "id",
                                Location(mLocationDisplay.location.position.x.toFloat(),
                                mLocationDisplay.location.position.y.toFloat()),
                                Date().time,
                                "https://i.imgur.com/1Gjbpgd.png",
                                null)

                        disposable =
                                eventsService.sendLocation(ally)
                                        .subscribeOn(Schedulers.io())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(
                                                {
                                                    result -> addAlly(result.data, myPersNumber)
                                                },
                                                {
                                                    error -> Toast.makeText(activity.baseContext, "error: ${error.message}", Toast.LENGTH_LONG).show()
                                                }
                                        )

//
//                        EventBus.getDefault().post(NewAllyEvent(ally))

                    }
                    R.id.focus_on_my_location_button -> {
                        // re-centers the map on the symbol.
                        mLocationDisplay.autoPanMode = LocationDisplay.AutoPanMode.RECENTER
                        if (!mLocationDisplay.isStarted)
                            mLocationDisplay.startAsync()
                        return
                    }
                }

            }

            override fun onAnimationRepeat(animation: Animation) {

            }
        })

        view.startAnimation(anim)
    }

    private fun addAlly(ally: Ally?, myPersNumber: String) {
        // Check if it's not me
        if (ally!!.persNumber != myPersNumber) addAllyToMap(ally!!)
    }

    lateinit var mMapView: MapView
    lateinit var mLocationDisplay: LocationDisplay

    val requestCode = 2
    val reqPermissions = Array<String>(2){ Manifest.permission.ACCESS_FINE_LOCATION; Manifest.permission.ACCESS_COARSE_LOCATION}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.map_fragment, container, false)
    }

    override fun onViewCreated(rootView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(rootView, savedInstanceState)

        // inflate MapView from layout
        mMapView = rootView.findViewById(R.id.map_view)

//        mMapView.setOn(object : View.OnDragListener {
//            override fun onDrag(p0: View?, p1: DragEvent?): Boolean {
//                dismissToolTips()
//                return true
//            }
//
//        })

        mMapView.onTouchListener = object : DefaultMapViewOnTouchListener(activity, mMapView) {
            override fun onSingleTapConfirmed(motionEvent: MotionEvent): Boolean {

                // get the point that was clicked and convert it to a point in map coordinates
                val screenPoint = android.graphics.Point(Math.round(motionEvent.x), Math.round(motionEvent.y))
                // create a map point from screen point
                val mapPoint = mMapView.screenToLocation(screenPoint)
                // convert to WGS84 for lat/lon format
                try {
                    val wgs84Point = GeometryEngine.project(mapPoint, SpatialReferences.getWgs84())
                } catch (e: Exception) {

                }

                dismissToolTips()

                if (true) {
                    createReportBubble(mapPoint, true)
                }

                return true
            }

            override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean {
                dismissToolTips()
                return super.onFling(e1, e2, velocityX, velocityY)
            }
        }


        val reportEventButton = rootView.findViewById<View>(R.id.report_event_button)
        val focusOnMyLocationButton = rootView.findViewById<View>(R.id.focus_on_my_location_button)

        focusOnMyLocationButton.setOnClickListener(this)
        reportEventButton.setOnClickListener(this)

        // get the MapView's LocationDisplay
        mLocationDisplay = mMapView.locationDisplay

        // Listen to changes in the status of the location data source.
        mLocationDisplay.addDataSourceStatusChangedListener(object : LocationDisplay.DataSourceStatusChangedListener {
            override fun onStatusChanged(dataSourceStatusChangedEvent: LocationDisplay.DataSourceStatusChangedEvent) {

                // If LocationDisplay started OK, then continue.
                if (dataSourceStatusChangedEvent.isStarted)
                  return

                // No error is reported, then continue.
                if (dataSourceStatusChangedEvent.error == null)
                  return

                // If an error is found, handle the failure to start.
                // Check permissions to see if failure may be due to lack of permissions.
                val permissionCheck1 = ContextCompat.checkSelfPermission(activity, reqPermissions[0]) ==
                    PackageManager.PERMISSION_GRANTED
                val permissionCheck2 = ContextCompat.checkSelfPermission(activity, reqPermissions[1]) ==
                    PackageManager.PERMISSION_GRANTED

                if (!(permissionCheck1 && permissionCheck2)) {
                  // If permissions are not already granted, request permission from the user.
                  ActivityCompat.requestPermissions(activity, reqPermissions, requestCode)
                } else {
                  // Report other unknown failure types to the user - for example, location services may not
                  // be enabled on the device.
                  val message = String.format("Error in DataSourceStatusChangedListener: %s", dataSourceStatusChangedEvent
                      .source.locationDataSource.error.message)
                  Toast.makeText(activity, message, Toast.LENGTH_LONG).show()

                  // Update UI to reflect that the location display did not actually start
//                  mSpinner.setSelection(0, true);
                }
            }
        })

        // re-centers the map on the symbol.
        mLocationDisplay.autoPanMode = LocationDisplay.AutoPanMode.RECENTER
        if (!mLocationDisplay.isStarted)
            mLocationDisplay.startAsync()

        //Create points to add graphics to the map to allow a renderer to style them
        //These are in WGS84 coordinates (Long, Lat)
//        val sagiHousePoint = Point(34.8235323,31.896263, SpatialReferences.getWgs84())
//        val secPoint = Point( 34.816523,31.898762, SpatialReferences.getWgs84())

        // create a map with the imagery basemap. This will set the map to have a WebMercator spatial reference
        utils = Utils(activity)
        utils.readPersNumberFromSharedPrefs()
        val map = ArcGISMap(utils.mapServerAddress)

        // set the map to be displayed in the mapview
        mMapView.map = map

//        pullAlliesFromServer()
//        pullLastEventsFromServer()

        //set initial envelope on the map view sith some padding so all points will be visible
        //This envelope is using the WGS84 points above, but is reprojected by the mapview into the maps spatial reference, so its works fine
//        val initialEnvelope = Envelope(secPoint, firstPoint)
//        mMapView.setViewpointGeometryAsync(initialEnvelope, 100.0)
    }

    private fun pullAlliesFromServer() {
        disposable =
                eventsService.getAllies()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    result -> addAllies(result.data)
                                },
                                {
                                    error -> Toast.makeText(activity.baseContext, "error: ${error.message}", Toast.LENGTH_LONG).show()
                                }
                        )
    }

    private fun addAllies(data: Array<Ally>?) {
        if (activity != null) {
            utils = Utils(activity)
            utils.readPersNumberFromSharedPrefs()

            val myPersNumber = utils.persNumber.toString()

            for (index in 0..data!!.size - 1) {
                val ally = data[index]

                // Check if it's not me
                if (ally.persNumber != myPersNumber) addAllyToMap(Ally(ally.persNumber, ally.id, ally.location, ally.timestamp, ally.url, ally.name))
            }
        }
    }

    private fun pullLastEventsFromServer() {
        disposable =
                eventsService.getEvents()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                {
                                    result -> updateEvents(result.data)
                                },
                                {
                                    error -> Toast.makeText(activity.baseContext, "error: ${error.message}", Toast.LENGTH_LONG).show()
                                }
                        )

        //showNoEventsText()
    }

    private fun dismissToolTips() {
        mMapView.callout.dismiss()
        (activity as MainActivity).dialogReportIsOpen = false
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onNewPointEvent(event: Any) {
        when (event) {
            is NewAllyEvent -> addAllyToMap(event.ally)
            is ChangeStrokeColorEvent -> changeBubbleStrokeColor(event.color)
            is AllyLocationUpdateEvent -> addAllyToMap(event.ally)
            is NewReportEvent -> addEventToMap(event.report)
            is CloseTooltip -> dismissToolTips()
        }
    }

    private fun updateEvents(data: Array<Event>?) {
        if (activity != null) {
            for (index in 0..data!!.size - 1) {
                addEventToMap(data[index])
            }
        }
    }

    private fun addAllyToMap(ally: Ally) {
        // Remove ally from the screen if exist
        // TODO: REMOVE ONLY THIS ALLY
        mMapView.graphicsOverlays.clear()

        // region Add Point To Screen

        // create a new graphics overlay and add it to the mapview
        val allyPoint = Point( ally.location.lat.toDouble(), ally.location.long.toDouble(), SpatialReferences.getWgs84())
        val graphicOverlay = GraphicsOverlay()

        mMapView.graphicsOverlays.add(graphicOverlay)

        //Create a picture marker symbol from a URL resource
        //When using a URL, you need to call load to fetch the remote resource
        var allySymbol = PictureMarkerSymbol(ally.url)

        //Optionally set the size, if not set the image will be auto sized based on its size in pixels,
        //its appearance would then differ across devices with different resolutions.
        allySymbol.height = 18f
        allySymbol.width = 18f
        allySymbol.loadAsync()

        allySymbol.addDoneLoadingListener(Runnable {
            //Once the symbol has loaded, add a new graphic to the graphic overlay
            val allyGraphic = Graphic(allyPoint, allySymbol)
            graphicOverlay.graphics.add(allyGraphic)

            // TODO: Check if the point is in the field of view
            if (true) {
                createAllyBubble(ally, allyPoint, true)
            }
        })

        //endregion
    }

    private val RED_URL: String = "https://upload.wikimedia.org/wikipedia/commons/f/f1/Ski_trail_rating_symbol_red_circle.png"
    private val ORANGE_URL: String = "http://www.clker.com/cliparts/W/i/K/w/1/D/glossy-orange-circle-icon-md.png"
    private val YELLOW_URL: String = "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/Lol_circle.png/479px-Lol_circle.png"

    private fun addEventToMap(event: Event) {
        //region Add Point To Screen

        val df = DecimalFormat("#.####")
//        df.roundingMode = RoundingMode.

        val lat = df.format(event.location.lat)
        val long = df.format(event.location.long)

        // create a new graphics overlay and add it to the mapview
        val eventPoint = Point(long.toDouble(),lat.toDouble(), SpatialReferences.getWgs84())
        val graphicOverlay = GraphicsOverlay()
        var url = ""

        mMapView.graphicsOverlays.add(graphicOverlay)

        when (event.severity) {
            1 -> url = RED_URL
            2 -> url = ORANGE_URL
            3 -> url = YELLOW_URL
        }

        var eventSymbol = PictureMarkerSymbol(url)

        //Optionally set the size, if not set the image will be auto sized based on its size in pixels,
        //its appearance would then differ across devices with different resolutions.
        eventSymbol.height = 18f
        eventSymbol.width = 18f
        eventSymbol.loadAsync()

        eventSymbol.addDoneLoadingListener(Runnable {
            //Once the symbol has loaded, add a new graphic to the graphic overlay
            val eventGraphic = Graphic(eventPoint, eventSymbol)
            graphicOverlay.graphics.add(eventGraphic)

            // TODO: Check if the point is in the field of view
            if (true) {
//                createEventBubbleTest(eventPoint, event, false)
            }
        })

        //endregion

    }



    private fun createEventBubbleTest(tapPoint: Point, event: Event, centerTheScreen: Boolean) {
        // create a textview for the callout
        val calloutContent = TextView(activity.baseContext)
        calloutContent.text = (event.location.toString())

        // get callout, set content and show
        val mCallout = mMapView.callout
        mCallout.style.backgroundColor = resources.getColor(R.color.dark_background)
        mCallout.style.borderColor = resources.getColor(R.color.report_gray)
        mCallout.style.borderWidth = 3
        mCallout.style.maxWidth = 300
        mCallout.location = tapPoint
        mCallout.content = calloutContent
        mCallout.content.setPadding(0,0,0,0)
        mCallout.show()

        // Center the screen
        if (centerTheScreen) mMapView.setViewpointCenterAsync(tapPoint)
    }

    private fun createEventBubble(tapPoint: Point, event: Event, centerTheScreen: Boolean) {
        // create a textview for the callout
        val calloutContent = EventView(activity.baseContext)
        calloutContent.title!!.setText(event.name)
        calloutContent.time!!.setText(Utils(activity).getDateString(event.timestamp))
        calloutContent.text!!.setText(event.description)

        // get callout, set content and show
        val mCallout = mMapView.callout
        mCallout.style.backgroundColor = resources.getColor(R.color.dark_background)
        mCallout.style.borderColor = resources.getColor(R.color.report_gray)
        mCallout.style.borderWidth = 3
        mCallout.style.maxWidth = 300
        mCallout.location = tapPoint
        mCallout.content = calloutContent
        mCallout.content.setPadding(0,0,0,0)
        mCallout.show()

        // Center the screen
        if (centerTheScreen) mMapView.setViewpointCenterAsync(tapPoint)
    }

    private fun createReportBubble(tapPoint: Point, centerTheScreen: Boolean) {
        // create a textview for the callout
        val calloutContent = ReportView(activity.baseContext)
        calloutContent.initLocation(tapPoint)

        // get callout, set content and show
        val mCallout = mMapView.callout
        mCallout.style.backgroundColor = resources.getColor(R.color.dark_background)
        mCallout.style.borderColor = resources.getColor(R.color.report_gray)
        mCallout.style.borderWidth = 3
        mCallout.style.maxWidth = 300
        mCallout.location = tapPoint
        mCallout.content = calloutContent
        mCallout.content.setPadding(0,0,0,0)
        mCallout.show()

        // Center the screen
        if (centerTheScreen) mMapView.setViewpointCenterAsync(tapPoint)

        (activity as MainActivity).dialogReportIsOpen = true
    }

    fun changeBubbleStrokeColor(color: Int) {
        // get callout, set content and show
        val mCallout = mMapView.callout
        mCallout.style.borderColor = resources.getColor(color)
        mCallout.show()
    }

    private fun createAllyBubble(ally: Ally, allyPoint: Point, centerTheScreen: Boolean) {
        // create a textview for the callout
        val calloutContent = TextView(activity.baseContext)
        calloutContent.setTextColor(Color.WHITE)
        calloutContent.setSingleLine()
        calloutContent.text = "S${ally.persNumber}"

        // get callout, set content and show
        val mCallout = mMapView.callout
        mCallout.style.backgroundColor = resources.getColor(R.color.dark_background)
        mCallout.style.borderColor = resources.getColor(R.color.ally_green)
        mCallout.style.borderWidth = 3
        mCallout.location = allyPoint
        mCallout.content = calloutContent
        mCallout.show()

        // Center the screen
        if (centerTheScreen) mMapView.setViewpointCenterAsync(allyPoint)
    }

    override fun onPause() {
        super.onPause()
        // pause MapView
        mMapView.pause()
    }

    override fun onResume() {
        super.onResume()
        // resume MapView
        mMapView.resume()
    }


    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    companion object {
        fun newInstance(): Fragment {
            val fragment = MapFragment()
            val args = Bundle()
//            args.putString()
            fragment.arguments = args

            return fragment
        }
    }
}