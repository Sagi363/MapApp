package matzpen.idf.mapapp

import android.app.Application
import android.content.Intent
import matzpen.idf.mapapp.Services.SocketService

/**
 * Created by sagihatzabi on 12/4/17.
 */
class MyApplication: Application() {
    override fun onCreate() {
        super.onCreate()

        startService(Intent(this, SocketService::class.java))
    }
}