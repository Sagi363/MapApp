package matzpen.idf.mapapp.Services;

/**
 * Created by sagihatzabi on 12/4/17.
 */

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;


public class SocketService extends Service {
    final String TAG="SocketService";
    SocketThread thread;

    private final IBinder mBinder = new MyBinder();
    public class MyBinder extends Binder {
        SocketService getService() {
            return SocketService.this;
        }
    }

    @Override
    public void onCreate(){
        super.onCreate();
        thread = new SocketThread();
        thread.start();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_STICKY;
    }


    @Override
    public IBinder onBind(Intent intent) {
        //TODO for communication return IBinder implementation
        return null;
    }

    @Override
    public void onDestroy(){
        thread.interrupt();
        super.onDestroy();
        Log.d(TAG,"onDestroy");
    }

}