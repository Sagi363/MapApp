package matzpen.idf.mapapp.Services;

import android.util.Log;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import matzpen.idf.mapapp.Ally;
import matzpen.idf.mapapp.AllyLocationUpdateEvent;
import matzpen.idf.mapapp.Event;
import matzpen.idf.mapapp.Events.NewReportEvent;
import matzpen.idf.mapapp.NewAllyEvent;
import okhttp3.OkHttpClient;

/**
 * Created by sagihatzabi on 12/4/17.
 */

public class SocketThread extends Thread {
    final String TAG="SocketThread";
    private Socket socket;
    private Gson gson = new Gson();


    public void run() {
        try {
            OkHttpClient okHttpClient = null;
            try {
                TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new java.security.cert.X509Certificate[]{};
                    }

                    @Override
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    }
                }};

                SSLContext sslContext = SSLContext.getInstance("SSL");
                sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

                okHttpClient = new OkHttpClient.Builder()
                        .hostnameVerifier(new HostnameVerifier() {
                            @Override
                            public boolean verify(String s, SSLSession sslSession) {
                                return true;
                            }
                        })
                        .sslSocketFactory(sslContext.getSocketFactory())
                        .build();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }

// default settings for all sockets
            IO.setDefaultOkHttpWebSocketFactory(okHttpClient);
            IO.setDefaultOkHttpCallFactory(okHttpClient);

// set as an option
            IO.Options opts = new IO.Options();
            opts.callFactory = okHttpClient;
            opts.webSocketFactory = okHttpClient;

            socket = IO.socket("http://mapapp.now.sh/", opts); //this gets connected
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "Start running");

        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                Log.d(TAG, "Connected to server");
            }

        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... arg0) {
                Log.d(TAG, "Disconnected from server");
            }

        });

        socket.on("newAlly", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject json = (JSONObject) args[0];
                JSONObject data = null;
                try {
                    data = json.getJSONObject("data");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Ally ally = gson.fromJson(data.toString(), Ally.class);
                EventBus.getDefault().post(new NewAllyEvent(ally));
                Log.d(TAG, "Handling newAlly");
            }
        });

        socket.on("allyLocationUpdate", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject json = (JSONObject) args[0];
                JSONObject data = null;
                try {
                    data = json.getJSONObject("data");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Ally ally = gson.fromJson(data.toString(), Ally.class);
                EventBus.getDefault().post(new AllyLocationUpdateEvent(ally));
            }
        });

        socket.on("newEvent", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject json = (JSONObject) args[0];
                JSONObject data = null;
                try {
                    data = json.getJSONObject("data");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Event event = gson.fromJson(data.toString(), Event.class);
                EventBus.getDefault().post(new NewReportEvent(event));
            }
        });

        Log.d(TAG, "Socket thread started");
        socket.connect();
    }

}
