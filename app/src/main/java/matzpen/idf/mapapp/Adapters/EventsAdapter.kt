package matzpen.idf.mapapp

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemAdapter
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractSwipeableItemViewHolder
import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemConstants
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultAction
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultActionDefault
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultActionRemoveItem
import matzpen.idf.mapapp.Events.RemovedItemsEvent
import org.greenrobot.eventbus.EventBus
import matzpen.idf.mapapp.EventsAdapter.Swipeable




/**
 * Created by sagihatzabi on 11/26/17.
 */
class EventsAdapter(val eventsList: MutableList<Event>, val context: Context) : RecyclerView.Adapter<EventsAdapter.MyViewHolder>(), SwipeableItemAdapter<EventsAdapter.MyViewHolder> {
    interface Swipeable : SwipeableItemConstants

    val utils = Utils(context = context)

    init {
        setHasStableIds(true) // this is required for swiping feature.
    }

    inner class MyViewHolder(view: View) : AbstractSwipeableItemViewHolder(view) {
        var name: TextView
        var icon: ImageView
        var date: TextView
        var description: TextView
        var background: ConstraintLayout

        init {
            name = view.findViewById(R.id.event_item_name)
            icon = view.findViewById(R.id.event_item_icon)
            date = view.findViewById(R.id.event_item_date)
            description = view.findViewById(R.id.event_item_description)
            background = view.findViewById(R.id.event_item_background)
        }

        override fun getSwipeableContainerView(): View? {
            return background
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.notifictions_event_item, parent, false)

        // MUST for Swipeable Item
        for (i in 0..eventsList.size - 1) { eventsList.get(i).listID = i.toLong() }

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val event = eventsList[position]
        holder.name.setText(event.name)
        holder.date.setText(utils.getDateString(event.timestamp))
        holder.description.setText(event.description)

        var resId = 0
        when (event.severity) {
            1 -> resId = R.drawable.event_red_circle
            2 -> resId = R.drawable.event_orange_circle
            3 -> resId = R.drawable.event_yellow_circle
        }

        if (resId != 0) holder.icon.setBackgroundResource(resId)
    }

    override fun getItemCount(): Int {
        return eventsList.size
    }

    override fun onSetSwipeBackground(holder: MyViewHolder?, position: Int, type: Int) {
        var bgRes: Int? = 0
        when (type) {
            SwipeableItemConstants.DRAWABLE_SWIPE_NEUTRAL_BACKGROUND -> bgRes = R.color.event_background
            SwipeableItemConstants.DRAWABLE_SWIPE_LEFT_BACKGROUND -> bgRes = R.color.event_swipe_background
            SwipeableItemConstants.DRAWABLE_SWIPE_RIGHT_BACKGROUND -> bgRes = R.color.event_swipe_background
        }

        holder!!.itemView.setBackgroundResource(bgRes!!)
    }

    override fun onSwipeItemStarted(holder: MyViewHolder?, position: Int) {
        notifyDataSetChanged()
    }

    override fun onGetSwipeReactionType(holder: MyViewHolder?, position: Int, x: Int, y: Int): Int {
        return SwipeableItemConstants.REACTION_CAN_SWIPE_BOTH_H
    }

    override fun onSwipeItem(holder: MyViewHolder?, position: Int, result: Int): SwipeResultAction {
        return if (result === SwipeableItemConstants.RESULT_CANCELED) {
            SwipeResultActionDefault()
        } else {
            MySwipeResultActionRemoveItem(this, position)
        }
    }

    override fun getItemId(position: Int): Long {
        return eventsList.get(position).listID // need to return stable (= not change even after position changed) value
    }

    internal class MySwipeResultActionRemoveItem(private val adapter: EventsAdapter, private val position: Int) : SwipeResultActionRemoveItem() {

        override fun onPerformAction() {
            adapter.eventsList.removeAt(position)
            adapter.notifyItemRemoved(position)

            if (adapter.eventsList.size <= 4) EventBus.getDefault().post(RemovedItemsEvent())
        }
    }
}