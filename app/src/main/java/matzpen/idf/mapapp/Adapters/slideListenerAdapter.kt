package matzpen.idf.mapapp

import android.view.View
import com.mancj.slideup.SlideUp
import matzpen.idf.mapapp.R.id.slide_notifications_bar_rect
import org.greenrobot.eventbus.EventBus

/**
 * Created by sagihatzabi on 11/26/17.
 */
class slideListenerAdapter: SlideUp.Listener {
    override fun onVisibilityChanged(visibility: Int) {
        if (visibility == View.GONE) {
            EventBus.getDefault().post(SliderClosedEvent())
        }
    }

    override fun onSlide(percent: Float) {
        EventBus.getDefault().post(ScrollingEvent(percent))
    }

}