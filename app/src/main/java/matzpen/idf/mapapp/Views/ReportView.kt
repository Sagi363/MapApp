package matzpen.idf.mapapp

import android.content.Context
import android.view.ViewGroup
import android.opengl.ETC1.getWidth
import android.opengl.ETC1.getHeight
import android.app.Activity
import android.graphics.*
import android.view.Display
import android.graphics.Paint.ANTI_ALIAS_FLAG
import android.view.View
import android.R.attr.thumbnail
import android.R.attr.description
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.R.attr.thumbnail
import android.R.attr.description
import android.R.attr.thumbnail
import android.R.attr.description
import android.widget.*
import kotlinx.android.synthetic.main.report_view.view.*
import matzpen.idf.mapapp.Fragments.DialogReportFragment
import org.greenrobot.eventbus.EventBus
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import android.util.Log
import android.support.v4.app.ActivityCompat
import com.esri.arcgisruntime.geometry.Point
import com.esri.arcgisruntime.geometry.SpatialReferences
import com.esri.arcgisruntime.geometry.GeometryEngine




/**
 * Created by sagihatzabi on 12/3/17.
 */
class ReportView : ConstraintLayout, View.OnClickListener {

    private var severity = 0
    private var title: TextView? = null
    private var reportButtonYellow: ImageButton? = null
    private var reportButtonOrange: ImageButton? = null
    private var reportButtonRed: ImageButton? = null
    private var reportTextYellow: TextView? = null
    private var reportTextOrange: TextView? = null
    private var reportTextRed: TextView? = null
    private var reportButtonSend: TextView? = null
    private var canSendReport = false
    private lateinit var mContext: Context
    private var location: Location = Location(0f,0f)

    private fun init() {
        View.inflate(context, R.layout.report_view, this)

        //Add missing top level attributes
        val padding = resources.getDimension(R.dimen.report_view_padding).toInt()
        setPadding(padding, padding, padding, padding)

        this.title = findViewById<TextView>(R.id.report_event_title)
        this.reportButtonYellow = findViewById<ImageButton>(R.id.report_event_button_yellow)
        this.reportButtonOrange =  findViewById<ImageButton>(R.id.report_event_button_orange)
        this.reportButtonRed =  findViewById<ImageButton>(R.id.report_event_button_red)
        this.reportTextYellow = findViewById<TextView>(R.id.report_event_button_yellow_text)
        this.reportTextOrange =  findViewById<TextView>(R.id.report_event_button_orange_text)
        this.reportTextRed =  findViewById<TextView>(R.id.report_event_button_red_text)
        this.reportButtonSend =  findViewById<Button>(R.id.report_event_button_send)

        this.reportButtonYellow!!.setOnClickListener(this)
        this.reportButtonOrange!!.setOnClickListener(this)
        this.reportButtonRed!!.setOnClickListener(this)
        this.reportTextYellow!!.setOnClickListener(this)
        this.reportTextOrange!!.setOnClickListener(this)
        this.reportTextRed!!.setOnClickListener(this)
        this.reportButtonSend!!.setOnClickListener(this)
    }

    constructor(context: Context) : super(context) {
        mContext = context
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        mContext = context
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        mContext = context
        init()
    }


    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.report_event_button_yellow_text,
            R.id.report_event_button_yellow -> {
                EventBus.getDefault().post(ChangeStrokeColorEvent(R.color.colorYellow))
                this.reportButtonSend!!.background = resources.getDrawable(R.drawable.report_event_button_send_yellow)
                this.reportButtonOrange!!.setGrayIcon()
                this.reportButtonRed!!.setGrayIcon()
                (view as ImageButton).setIcon()
                canSendReport = true
                severity = 3
                return
            }
            R.id.report_event_button_orange_text,
            R.id.report_event_button_orange -> {
                EventBus.getDefault().post(ChangeStrokeColorEvent(R.color.colorOrange))
                this.reportButtonSend!!.background = resources.getDrawable(R.drawable.report_event_button_send_orange)
                this.reportButtonYellow!!.setGrayIcon()
                this.reportButtonRed!!.setGrayIcon()
                (view as ImageButton).setIcon()
                canSendReport = true
                severity = 2
                return
            }
            R.id.report_event_button_red_text,
            R.id.report_event_button_red -> {
                EventBus.getDefault().post(ChangeStrokeColorEvent(R.color.colorRed))
                this.reportButtonSend!!.background = resources.getDrawable(R.drawable.report_event_button_send_red)
                this.reportButtonYellow!!.setGrayIcon()
                this.reportButtonOrange!!.setGrayIcon()
                (view as ImageButton).setIcon()
                canSendReport = true
                severity = 1
                return
            }
            R.id.report_event_button_send -> {
                if (canSendReport) {
                    sendReport()
                }
                return
            }
        }
    }

    private fun sendReport() {
        if (severity != 0){
            // Eventbus
            EventBus.getDefault().post(OpenDialogReport(severity, location))
        }
        else {
            Toast.makeText(context, "severity == 0 - ERROR", Toast.LENGTH_SHORT).show()
        }

        //Toast.makeText(context, "Report", Toast.LENGTH_SHORT).show()
    }

    fun initLocation(tapPoint: Point) {
        val wgs84Point = GeometryEngine.project(tapPoint, SpatialReferences.getWgs84()) as Point
        location = Location(String.format("%.4f", wgs84Point.getY()).toFloat(), String.format("%.4f",
                wgs84Point.getX()).toFloat())
    }
}

private fun ImageButton.setGrayIcon() {
    this.setBackgroundResource(R.drawable.report_event_gray)
}

private fun ImageButton.setIcon() {
    when (this.id) {
        R.id.report_event_button_yellow -> {
            this.setBackgroundResource(R.drawable.report_event_yellow)
        }
        R.id.report_event_button_orange -> {
            this.setBackgroundResource(R.drawable.report_event_orange)
        }
        R.id.report_event_button_red -> {
            this.setBackgroundResource(R.drawable.report_event_red)
        }
    }
}
