package matzpen.idf.mapapp

import android.content.Context
import android.view.ViewGroup
import android.opengl.ETC1.getWidth
import android.opengl.ETC1.getHeight
import android.app.Activity
import android.graphics.*
import android.view.Display
import android.graphics.Paint.ANTI_ALIAS_FLAG
import android.view.View
import android.R.attr.thumbnail
import android.R.attr.description
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.R.attr.thumbnail
import android.R.attr.description
import android.R.attr.thumbnail
import android.R.attr.description
import android.widget.*
import kotlinx.android.synthetic.main.report_view.view.*
import org.greenrobot.eventbus.EventBus


/**
 * Created by sagihatzabi on 12/3/17.
 */
class EventView : ConstraintLayout{

    var title: TextView? = null
    var time: TextView? = null
    var text: TextView? = null

    private fun init() {
        View.inflate(context, R.layout.event_tooltip_view, this)

        //Add missing top level attributes
        val padding = resources.getDimension(R.dimen.report_view_padding).toInt()
        setPadding(padding, padding, padding, padding)

        this.title = findViewById<TextView>(R.id.report_event_title)
        this.time =  findViewById<TextView>(R.id.event_tooltip_time)
        this.text =  findViewById<TextView>(R.id.event_tooltip_text)
    }

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init()
    }

}