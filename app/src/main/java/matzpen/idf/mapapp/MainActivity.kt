package matzpen.idf.mapapp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.mancj.slideup.SlideUp

import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import android.view.View.*
import android.view.*
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_main.*
import matzpen.idf.mapapp.Fragments.DialogReportFragment
import matzpen.idf.mapapp.WebServices.pullEventsService


class MainActivity : AppCompatActivity(), OnClickListener {

    lateinit var slideUp: SlideUp
    lateinit var utils: Utils
    var dialogReportIsOpen = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        setSupportActionBar(toolbar)

        val slideNotifications = findViewById<View>(R.id.slide_notifications_layout)
        val slideNotificationsBar = findViewById<View>(R.id.slide_notifications_bar_layout)

        slideNotificationsBar.setOnClickListener(this)

        //region TEMP - ONLY FOR PERSONAL NUMBER
        val pers_number = findViewById<EditText>(R.id.pers_number)
        val server_address = findViewById<EditText>(R.id.server_address)

        utils = Utils(this)
        utils.readPersNumberFromSharedPrefs()

        if (utils.persNumber != null) {
            pers_number.visibility = GONE
        }

        if (utils.serverAddress != null) {
            server_address.visibility = GONE
        }

        if (utils.mapServerAddress != null) {
            map_address.visibility = GONE
        }

        if (utils.persNumber != null &&
                utils.mapServerAddress != null &&
                utils.serverAddress != null) {
            save.visibility = GONE

            // Create fragment transaction
            val transaction = fragmentManager.beginTransaction()

            // Add the map fragment
            val mapFragment = MapFragment.newInstance()
            transaction.replace(R.id.map_fragment_frame, mapFragment).commit()

            pullEventsService.initServerAddress(server_address.text.toString())
        }

        save.setOnClickListener { view ->
            utils.serverAddress = server_address.text.toString()
            utils.persNumber = pers_number.text.toString()
            utils.mapServerAddress = map_address.text.toString()
            utils.wtirePersNumberToSharedPrefs()
            pers_number.visibility = GONE
            server_address.visibility = GONE
            map_address.visibility = GONE

            // Create fragment transaction
            val transaction = fragmentManager.beginTransaction()

            // Add the map fragment
            val mapFragment = MapFragment.newInstance()
            transaction.replace(R.id.map_fragment_frame, mapFragment).commit()

            pullEventsService.initServerAddress(server_address.text.toString())
        }

//        server_address.setOnKeyListener { view: View?, i: Int, keyEvent: KeyEvent? ->
//            when (keyEvent!!.keyCode) {
//                KeyEvent.KEYCODE_ENTER -> {
//                    utils.serverAddress = (view as EditText).text.toString()
//                    utils.wtirePersNumberToSharedPrefs()
//                    server_address.visibility = GONE
//                    pullEventsService.initServerAddress((view as EditText).text.toString())
//                    true
//                }
//            }
//
//            false
//        }
//
//        pers_number.setOnKeyListener { view: View?, i: Int, keyEvent: KeyEvent? ->
//            when (keyEvent!!.keyCode) {
//                KeyEvent.KEYCODE_ENTER -> {
//                    utils.persNumber = (view as EditText).text.toString()
//                    utils.wtirePersNumberToSharedPrefs()
//                    pers_number.visibility = GONE
//                    true
//                }
//            }
//
//            false
//        }
        //endregion

        slideUp = SlideUp.Builder(slideNotifications)
                .withStartState(SlideUp.State.HIDDEN)
                .withStartGravity(Gravity.TOP)
                .withListeners(slideListenerAdapter())
                .build()

        slideNotificationsBar.setOnClickListener { view ->
            slideUp.show()
        }

        slideNotificationsBar.setOnTouchListener { view, motionEvent ->

            if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                slideUp.show()
            }

            true
        }

        // Create fragment transaction
        val transaction = fragmentManager.beginTransaction()

        // Add the events fragment
        val eventsFragment = EventsFragment.newInstance()

        transaction.replace(R.id.events_fragment_frame, eventsFragment).commit()

    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.slide_notifications_bar_layout -> {
                slideUp.show()
            }
        }
    }

    fun changeBarSize(percent: Float) {
        val bar: View = findViewById<View>(R.id.slide_notifications_bar_rect)
        val slideNotifications = findViewById<View>(R.id.slide_notifications_layout)

        if (percent > 10) {
            toggleNotificationsBar(false)
        }

        bar.alpha = (1 - (percent / 100))
        bar.scaleX = (1 - (percent / 100))
        slideNotifications.alpha = (1.7f - (percent / 100))
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onScrollingEvent(event: Any) {
        when (event) {
            is ScrollingEvent -> {
                changeBarSize(event.percent)
            }
            is SliderClosedEvent -> {
                toggleNotificationsBar(true)
            }
            is OpenDialogReport -> openDialogReport(event.severity, event.location)
        }
    }

    private fun openDialogReport(severity: Int, location: Location) {
        // Create fragment transaction
//        val transaction = fragmentManager.beginTransaction()

        var dialogFragment = DialogReportFragment.newInstance(severity, location)
        dialogFragment.show(supportFragmentManager,"DialogReportFragment")
    }

    private fun toggleNotificationsBar(show: Boolean) {
        val slideNotificationsBar = findViewById<View>(R.id.slide_notifications_bar_layout)

        if (show) {
            slideNotificationsBar.visibility = VISIBLE

        }
        else {
            slideNotificationsBar.visibility = GONE
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onBackPressed() {
        if (dialogReportIsOpen) {
            // Close the dialog report windows
            EventBus.getDefault().post(CloseTooltip())
        }
        else {
            super.onBackPressed()
        }
    }
}
