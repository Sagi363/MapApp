package matzpen.idf.mapapp

import android.content.Context
import java.util.*

/**
 * Created by sagihatzabi on 11/27/17.
 */
class Utils(context: Context) {

    val mContext = context
    var persNumber: String? = null
    var serverAddress: String? = null
    var mapServerAddress: String? = null

    fun getDateString(date: Long): String{
        var dateString = ""

        val diff = date- Date().time
        var diffSeconds = diff / 1000 % 60
        var diffMinutes = diff / (60 * 1000) % 60
        var diffHours = diff / (60 * 60 * 1000)
        var diffInDays = ((date - Date().time) / (1000 * 60 * 60 * 24)).toInt()

        diffSeconds = 0 - diffSeconds
        diffMinutes = 0 - diffMinutes
        diffHours = 0 - diffHours
        diffInDays = 0 - diffInDays

        if (diffInDays > 0) {
            if (diffInDays == 1) {
                dateString = mContext.getString(R.string.one_day_ago_string)
            } else {
                dateString = mContext.getString(R.string.x_day_ago_string, diffInDays)
            }
        }
        else if (diffHours > 0) {
            if (diffHours == 1L) {
                dateString = mContext.getString(R.string.one_hour_ago_string)
            } else {
                dateString = mContext.getString(R.string.x_hours_ago_string, diffHours)
            }
        }
        else if (diffMinutes > 0) {
            if (diffMinutes == 1L) {
                dateString = mContext.getString(R.string.one_minute_ago_string)
            } else {
                dateString = mContext.getString(R.string.x_minutes_ago_string, diffMinutes)
            }
        }
        else {
            dateString = mContext.getString(R.string.x_seconds_ago_string, diffSeconds)
        }

        return dateString
    }

    fun wtirePersNumberToSharedPrefs() {
        val sharedPref = mContext!!.getSharedPreferences(mContext!!.getString(R.string.app_name), Context.MODE_PRIVATE)
        val editor = sharedPref.edit()

        editor.putString(mContext!!.getString(R.string.shared_pref_key), persNumber)
        editor.putString(mContext!!.getString(R.string.shared_pref_key2), serverAddress)
        editor.putString(mContext!!.getString(R.string.shared_pref_key3), mapServerAddress)
        editor.commit()
    }

    fun readPersNumberFromSharedPrefs() {
        val sharedPref = mContext!!.getSharedPreferences(mContext!!.getString(R.string.app_name), Context.MODE_PRIVATE)
        persNumber = sharedPref.getString(mContext!!.getString(R.string.shared_pref_key), null)
        serverAddress = sharedPref.getString(mContext!!.getString(R.string.shared_pref_key2), null)
        mapServerAddress = sharedPref.getString(mContext!!.getString(R.string.shared_pref_key3), null)
    }

}