package matzpen.idf.mapapp

/**
 * Created by sagihatzabi on 11/8/17.
 */
class OpenDialogReport(val severity: Int, val location: Location)